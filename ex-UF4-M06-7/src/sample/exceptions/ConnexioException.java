package sample.exceptions;

/**
 * @author Francesc Banzo
 * @author David Ortega
 *
 */
public class ConnexioException extends Exception {

    public ConnexioException(String msg) {
        super(msg);
    }
}
