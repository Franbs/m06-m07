package sample.interfaces;

import sample.clases.Anime;
import sample.clases.Studio;

import java.util.List;

/**
 * @author Francesc Banzo
 * @author David Ortega
 *
 */
public interface DAOStudio extends DAO<Studio, Integer> {

    public void insertAnime(Anime anime);

    public void deleteAnime(Studio studio, Anime anime);

    public void updateAnime(Anime anime);

    public List<Anime> searchByAnimeName(Studio studio, String name);

    public List<Studio> searchByStudioName(String name);

    //public List<Anime> searchAnimeNotFinished(Studio studio);

    //public boolean comprobarIdAnime(int id);
}
