package sample.interfaces;

import java.util.List;

/**
 * @author Francesc Banzo
 * @author David Ortega
 *
 */
public interface DAO<T, I> {

    public void insert(T studio);

    public void delete(T studio);

    public void update(T studio);

    public List<T> getAll();

    public T getByIdentifier(I id);
}