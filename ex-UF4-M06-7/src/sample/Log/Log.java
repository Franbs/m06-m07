package sample.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class Log {
    static Logger logger = Logger.getLogger("LOG");
    static FileHandler fileHandler;

    public static void save(String text, Exception throwables) {
        try {
            FileOutputStream fos = new FileOutputStream(new File("logFile.log"), true);
            PrintStream ps = new PrintStream(fos);
            ps.println(text);
            throwables.printStackTrace(ps);
            ps.println("------------------------------------------------------------------------------------------------------------\n");
            fos.close();

        } catch (SecurityException e) {
            e.printStackTrace();

        } catch (IOException e) {
            e.printStackTrace();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
