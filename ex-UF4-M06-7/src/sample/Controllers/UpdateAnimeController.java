package sample.Controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import sample.Log.Log;
import sample.clases.Anime;
import sample.clases.Studio;

import java.io.IOException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

/**
 * @author Francesc Banzo
 * @author David Ortega
 *
 */
public class UpdateAnimeController {
    Anime anime = new Anime();
    public boolean enProduccioSelectedTrue;

    @FXML
    private Button btDesa;

    @FXML
    private Button btCancela;

    @FXML
    private TextField idStudio;

    @FXML
    private TextField idAnime;

    @FXML
    private TextField nomAnime;

    @FXML
    private DatePicker dataInici;

    @FXML
    private RadioButton enProduccioTrue;

    @FXML
    private RadioButton enProduccioFalse;

    /**
     * El que pasara cada vegada que s'entri en el fxml
     */
    public void initialize() {
        try {
            // Agafem l'estudi amb l'id declarat de en el maincontroller
            Studio studio = Main.gestioPersistencia.getByIdentifier(MainController.idStudio);
            // Agafem l'anime en el que el seu id coincideixi amb el id d'anime declarat public static en updatestudiocontroller
            for (Anime a : studio.getLlistaAnimes()) {
                if (a.getId() == UpdateStudioController.idAnime) {
                    anime = a;
                }
            }

            // Se li fiquen les dades noves a l'anime per que es vegin les dades quan obres la pantalla d'editar anime
            idStudio.setText(String.valueOf(MainController.idStudio));
            idAnime.setText(String.valueOf(UpdateStudioController.idAnime));
            nomAnime.setText(anime.getName());

            // En cas de ser un jdbc pasa la data tipus sql date a un localdate i en cas de mongo pasa tipus date a localdate
            Date data = anime.getDataInici();
            if (DatabaseController.conexioMongoDB != null) {
                LocalDate date = data.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
                dataInici.setValue(date);
            }

            if (DatabaseController.conexioJDBC != null) {
                dataInici.setValue(Instant.ofEpochMilli(data.getTime()).atZone(ZoneId.systemDefault()).toLocalDate());
            }

            // Depenent de si esta en produccio o no l'anime, es preselecciona n radio button diferent
            if (anime.isEnProduccio()) {
                enProduccioTrue.setSelected(true);
                enProduccioSelectedTrue = true;
            } else {
                enProduccioFalse.setSelected(true);
                enProduccioSelectedTrue = false;
            }

        } catch (Exception e) {
            System.err.println("initialize UpdateAnimeController: " + e);

            String text = "initialize UpdateAnimeController: \n";
            Log.save(text, e);
        }
    }

    /**
     * @param actionEvent
     * @throws IOException
     * Cancela l'operacio
     */
    @FXML
    public void clickCancela(ActionEvent actionEvent) throws IOException {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("updateStudio.fxml"));
            Stage stage = (Stage) btCancela.getScene().getWindow();
            Scene scene = new Scene(loader.load());
            stage.setTitle("Editar estudi");
            stage.setScene(scene);

        } catch (IOException io) {
            //io.printStackTrace();
            System.err.println("clickCancela UpdateAnimeController: " + io);

            String text = "clickCancela UpdateAnimeController: \n";
            Log.save(text, io);

        } catch (Exception e) {
            System.err.println("clickCancela UpdateAnimeController: " + e);

            String text = "clickCancela UpdateAnimeController: \n";
            Log.save(text, e);
        }
    }

    /**
     * @param actionEvent
     * @throws IOException
     * Guarda les dades noves de l'anime escollit i l'updateja
     */
    @FXML
    public void clickDesaAnime(ActionEvent actionEvent) throws IOException {
        try {
            // Es guarden les noves dades a l'obj anime
            anime.setId(Integer.parseInt(idAnime.getText()));
            anime.setName(nomAnime.getText());

            LocalDate localDate = dataInici.getValue();
            Instant instant = Instant.from(localDate.atStartOfDay(ZoneId.systemDefault()));
            Date dateInici = Date.from(instant);
            anime.setDataInici(dateInici);

            anime.setIdStudio(MainController.idStudio);

            Studio studio = Main.gestioPersistencia.getByIdentifier(MainController.idStudio);

            if (enProduccioTrue.isSelected() && enProduccioSelectedTrue == false) {
                anime.setEnProduccio(true);

                // Se li sumara 1 als animes en produccio de l'estudi
                int num = studio.getAnimesEnProduccio() + 1;
                studio.setAnimesEnProduccio(num);
                // S'updateja l'estudi
                Main.gestioPersistencia.update(studio);
            }

            if (enProduccioFalse.isSelected() && enProduccioSelectedTrue == true) {
                anime.setEnProduccio(false);

                int num = studio.getAnimesEnProduccio() - 1;
                studio.setAnimesEnProduccio(num);
                Main.gestioPersistencia.update(studio);
            }

            // S'updateja l'anime
            Main.gestioPersistencia.updateAnime(anime);

            FXMLLoader loader = new FXMLLoader(getClass().getResource("updateStudio.fxml"));
            Stage stage = (Stage) btDesa.getScene().getWindow();
            Scene scene = new Scene(loader.load());
            stage.setTitle("Pantalla principal");
            stage.setScene(scene);

        } catch (IOException io) {
            //io.printStackTrace();
            System.err.println("clickDesaAnime UpdateAnimeController: " + io);

            String text = "clickDesaAnime UpdateAnimeController: \n";
            Log.save(text, io);

        } catch (Exception e) {
            System.err.println("clickDesaAnime UpdateAnimeController: " + e);

            String text = "clickDesaAnime UpdateAnimeController: \n";
            Log.save(text, e);
        }
    }
}
