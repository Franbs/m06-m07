package sample.DAO;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.result.DeleteResult;
import org.bson.Document;
import sample.Log.Log;
import sample.clases.Anime;
import sample.clases.Studio;
import sample.exceptions.ClauException;
import sample.interfaces.DAOStudio;

import java.util.ArrayList;
import java.util.List;

import static sample.Controllers.DatabaseController.conexioMongoDB;

/**
 * @author Francesc Banzo
 *
 */
public class DAOStudioMongoDB implements DAOStudio {
    /**
     * @param studio Studio que insertarem en la db
     * Inserta un studio en la db
     */
    @Override
    public void insert(Studio studio) {
        // S'obre la connexio
        conexioMongoDB.open();

        // S'agafa la db i la coleccio i es crea el doc amb els valors
        MongoDatabase db = conexioMongoDB.getDatabase();

        try {
            // Agafem tots els estudis
            List<Studio> llistaEstudios = getAll();
            // Boolean per comprovar que l'estudi no esta repetit
            boolean estudioRepetit = false;

            MongoCollection<Document> collection = db.getCollection("studios");
            Document doc = new Document("id", studio.getId()).append("name", studio.getName()).append("numTreballadors", studio.getNumTreballadors()).append("animesEnProduccio", studio.getAnimesEnProduccio());

            // Comprobem que no existeix cap estudi amb la id del estudi que hem d'insertar
            for (Studio s : llistaEstudios) {
                if (s.getId() == studio.getId()) {
                    estudioRepetit = true;
                    throw new ClauException("El id no pot ser el mateix");
                    //break;
                }
            }

            if (!estudioRepetit) {
                if (studio.getLlistaAnimes() != null) {
                    List<Document> llistaAnimes = new ArrayList<>();
                    for (Anime anime : studio.getLlistaAnimes()) {
                        llistaAnimes.add(new Document("id", anime.getId()).append("name", anime.getName()).append("dataInici", anime.getDataInici()).append("enProduccio", anime.isEnProduccio()).append("idStudio", anime.getIdStudio()));
                    }

                    doc.append("animes", llistaAnimes);
                }

                collection.insertOne(doc);
            }

        } catch (ClauException e) {
            System.err.println(e);

            String text = "insert DAOStudioJDBC: \n";
            Log.save(text, e);

        } catch (Exception e) {
            //e.printStackTrace();
            System.err.println("insert DAOStudioMongoDB: " + e);

            String text = "insert DAOStudioMongoDB: \n";
            Log.save(text, e);
        } finally {
            conexioMongoDB.close();
        }
    }

    /**
     * @param studio Studio que eliminarem
     * Elimina un studio de la db
     */
    @Override
    public void delete(Studio studio) {
        conexioMongoDB.open();

        MongoDatabase db = conexioMongoDB.getDatabase();

        try {
            MongoCollection<Document> collection = db.getCollection("studios");

            // S'elimina l'estudi que tingui l'id de l'estudi introduit
            DeleteResult deleteResult = collection.deleteOne(new Document("id", studio.getId()));
        } catch (Exception e) {
            //e.printStackTrace();
            System.err.println("delete DAOStudioMongoDB: " + e);

            String text = "delete DAOStudioMongoDB: \n";
            Log.save(text, e);
        } finally {
            conexioMongoDB.close();
        }
    }

    /**
     * @param anime Anime que insertarem en la db
     * Inserta un animee en la db
     */
    @Override
    public void insertAnime(Anime anime) {
        conexioMongoDB.open();

        MongoDatabase db = conexioMongoDB.getDatabase();

        // Agafem l'estudi amb l'id de l'estudi que te l'obj anime
        Studio studio = getByIdentifier(anime.getIdStudio());

        boolean animeRepetit = false;

        List<Anime> llistaAnimes = studio.getLlistaAnimes();

        try {
            // Es recorre la llista d'animes que previament hem agafat
            for (Anime a : llistaAnimes) {
                // Si l'id del obj de la llista es igual al de l'obj pasat per parametre,
                // no s'introdueix l'anime ja que ja hi ha un en la bd amb el mateix id
                if (a.getId() == anime.getId()) {
                    animeRepetit = true;
                    throw new ClauException("L'id no pot ser el mateix");
                    //break;
                }
            }

            //if (!animeRepetit) {
                studio.getLlistaAnimes().add(anime);
                delete(studio);
                insert(studio);
            /*} else {
                // Si l'anime esta en produccio
                if (anime.isEnProduccio()) {
                    // Si l'anime ja esta introduit li restem 1 als animes en produccio perque si no, no s'afegia l'nime pero s'incrementava igual
                    int num = studio.getAnimesEnProduccio() - 1;
                    studio.setAnimesEnProduccio(num);
                    update(studio);
                }
                System.out.println("L'anime introduit ja esta en la bd");
            }*/

            //replaceOne(Filters.eq("id", studio.getId()));
        } catch (ClauException e) {
            System.err.println(e);

            int num = studio.getAnimesEnProduccio() - 1;
            studio.setAnimesEnProduccio(num);
            update(studio);

            String text = "insert DAOStudioJDBC: \n";
            Log.save(text, e);

        } catch (Exception e) {
            //e.printStackTrace();
            System.err.println("insertAnime DAOStudioMongoDB: " + e);

            String text = "insertAnime DAOStudioMongoDB: \n";
            Log.save(text, e);

        } finally {
            conexioMongoDB.close();
        }
    }

    /**
     * @param studio Studio que actualitzarem quan s'hagi eliminat l'anime de la seva llista
     * @param anime Anime que eliminarem de la db
     * Elimina un anime de la db
     */
    @Override
    public void deleteAnime(Studio studio, Anime anime) {
        conexioMongoDB.open();

        MongoDatabase db = conexioMongoDB.getDatabase();

        try {
            MongoCollection<Document> collection = db.getCollection("studios");

            // S'agafa la llista d'animes de l'estudi pasat per parametre i se li elimina l'anime pasat
            studio.getLlistaAnimes().remove(anime);

            // S'updateja l'estudi
            delete(studio);
            insert(studio);
        } catch(Exception e) {
            //e.printStackTrace();
            System.err.println("deleteAnime DAOStudioMongoDB: " + e);

            String text = "deleteAnime DAOStudioMongoDB: \n";
            Log.save(text, e);
        } finally {
            conexioMongoDB.close();
        }
    }

    /**
     * @param anime Anime que s'updatejara
     * Updateja un anime de la db
     */
    @Override
    public void updateAnime(Anime anime) {
        int idStudio = anime.getIdStudio();

        // Agafem l'estudi amb el id de l'estudi que te l'anime que li pasem
        Studio studio = getByIdentifier(idStudio);

        // Per cada anime dins de la llista d'animes de l'estudi
        for (Anime a : studio.getLlistaAnimes()) {

            // Si l'id de l'obj de la llista es igual al de l'obj que li pasem
            if (a.getId() == anime.getId()) {

                // S'elimina de la llista l'anime que ja esta i se li inserta el nou anime
                studio.getLlistaAnimes().remove(a);
                studio.getLlistaAnimes().add(anime);

                // S'updateja l'estudi
                delete(studio);
                insert(studio);
            }
        }
    }

    /**
     * @param studio Studio que actualitzarem
     * Updateja un studio de la db
     */
    @Override
    public void update(Studio studio) {
        conexioMongoDB.open();

        MongoDatabase db = conexioMongoDB.getDatabase();

        try {
            MongoCollection<Document> collection = db.getCollection("studios");

            // Agafem els estudis de la bd
            List<Studio> llistaStudios = getAll();

            // Per cada estudi de la llista
            for (Studio s : llistaStudios) {
                // Si l'id de l'obj de la llista es igual a l'id de l'estudi que li pasem
                if (s.getId() == studio.getId()) {
                    delete(s);
                    insert(studio);
                }
            }

            //replaceOne(Filters.eq("id", studio.getId()));
        } catch(Exception e) {
            //e.printStackTrace();
            System.err.println("update DAOStudioMongoDB: " + e);

            String text = "update DAOStudioMongoDB: \n";
            Log.save(text, e);
        } finally {
            conexioMongoDB.close();
        }
    }

    /**
     * Recupera tots els studios de la db amb la seva info
     */
    @Override
    public List<Studio> getAll() {
        List<Studio> estudios = new ArrayList<>();

        conexioMongoDB.open();

        MongoDatabase db = conexioMongoDB.getDatabase();

        try {
            MongoCollection<Document> collection = db.getCollection("studios");

            // Declaras el cursor
            try (MongoCursor<Document> cursor = collection.find().cursor();) {

                // Mentre hi hagi documents
                while (cursor.hasNext()) {
                    Studio studio = new Studio();

                    // Declarem un doc que tindra els valors del cursor en la seguent posicio
                    Document doc = cursor.next();

                    // Agafem els valors
                    studio.setId(doc.getInteger("id"));
                    studio.setName(doc.getString("name"));
                    studio.setNumTreballadors(doc.getInteger("numTreballadors"));
                    studio.setAnimesEnProduccio(doc.getInteger("animesEnProduccio"));

                    // Si la llista d'animes no es igual a null
                    if (doc.getList("animes", Document.class) != null) {
                        List<Anime> animes = new ArrayList<>();

                        // Declarem una llista de docs que agafa la llista del document
                        List<Document> llistaAnimes = doc.getList("animes", Document.class);
                        // Per cada doc d'aquesta llista
                        for (Document doc2 : llistaAnimes) {
                            // Es crea un nou anime amb les dades de l'anime del doc
                            Anime a = new Anime(doc2.getInteger("id"), doc2.getString("name"), doc2.getDate("dataInici"), doc2.getBoolean("enProduccio"), doc2.getInteger("idStudio"));
                            animes.add(a);
                        }

                        studio.setLlistaAnimes(animes);
                    }

                    estudios.add(studio);
                }
            }

        } catch(NullPointerException e) {
            //e.printStackTrace();
            System.err.println("getAll DAOStudioMongoDB: " + e);

            String text = "getAll DAOStudioMongoDB: \n";
            Log.save(text, e);

        } catch(Exception e) {
            //e.printStackTrace();
            System.err.println("getAll DAOStudioMongoDB: " + e);

            String text = "getAll DAOStudioMongoDB: \n";
            Log.save(text, e);

        } finally {
            conexioMongoDB.close();
        }

        return estudios;
    }

    /**
     * @param id Id que farem servir per trobar un studio en la db
     * Recupera un studio de la db mitjanšant un id
     */
    @Override
    public Studio getByIdentifier(Integer id) {
        List<Studio> estudiosId = getAll();

        Studio studio = null;

        conexioMongoDB.open();

        MongoDatabase db = conexioMongoDB.getDatabase();

        try {
            MongoCollection<Document> collection = db.getCollection("studios");

            // Per cada estudi de la llista que conte tots els estudis
            for (Studio s : estudiosId) {
                // Quan coincideixin els ids
                if (s.getId() == id) {
                    studio = s;
                }
            }

        } catch(Exception e) {
            //e.printStackTrace();
            System.err.println("getByIdentifier DAOStudioMongoDB: " + e);

            String text = "getByIdentifier DAOStudioMongoDB: \n";
            Log.save(text, e);

        } finally {
            conexioMongoDB.close();
        }

        return studio;
    }

    /**
     * @param studio Studio que farem servir per agafar la llista d'animes
     * @param name Nom que farem servir per buscar animes en la db
     * Recupera una llista d'animes que tenen el nom que se li pasa per parametre
     * 
     */
    @Override
    public List<Anime> searchByAnimeName(Studio studio, String name) {
        List<Anime> animes = new ArrayList<>();

        conexioMongoDB.open();

        MongoDatabase db = conexioMongoDB.getDatabase();

        try {
            // Per cada anime en la llista de l'estudi que li pases
            for (Anime anime : studio.getLlistaAnimes()) {
                // Si els noms son iguals s'afegeix l'anime a la llista vuida que hem fet previament
                if (anime.getName().equalsIgnoreCase(name)) {
                    animes.add(anime);
                }
            }

        } catch(Exception e) {
            //e.printStackTrace();
            System.err.println("searchByAnimeName DAOStudioMongoDB: " + e);

            String text = "searchByAnimeName DAOStudioMongoDB: \n";
            Log.save(text, e);

        } finally {
            conexioMongoDB.close();
        }

        return animes;
    }

    /**
     * @param name Nom que farem servir per buscar
     * Recupera un llista de studios que tenen el nom que li pases
     */
    @Override
    public List searchByStudioName(String name) {
        List<Studio> studios = getAll();
        List<Studio> studiosNames = new ArrayList<>();

        Studio studio = null;

        conexioMongoDB.open();

        MongoDatabase db = conexioMongoDB.getDatabase();

        try {
            MongoCollection<Document> collection = db.getCollection("studios");

            // Per cada estudi en la llista de estudis que conte tots els estudis de la bd
            for (Studio s : studios) {
                // Si els noms coincideixen s'afegeix a la llista vuida l'estudi
                if (s.getName().equalsIgnoreCase(name)) {
                    studio = s;
                    studiosNames.add(studio);
                }
            }

        } catch(Exception e) {
            //e.printStackTrace();
            System.err.println("searchByStudioName DAOStudioMongoDB: " + e);

            String text = "searchByStudioName DAOStudioMongoDB: \n";
            Log.save(text, e);

        } finally {
            conexioMongoDB.close();
        }

        return studiosNames;
    }

    /*@Override
    public List<Anime> searchAnimeNotFinished(Studio studio) {
        return null;
    }*/

    /*@Override
    public boolean comprobarIdAnime(int id) {
        boolean animeRepetit = false;

        /*List<Studio> llistaStudios = dbMongo.getAll();

        for (Studio s : llistaStudios) {
            for (Anime a : s.llistaAnimes) {
                if (a.id == id) {
                    animeRepetit = true;
                }
            }
        }
        return animeRepetit;
    }*/
}
