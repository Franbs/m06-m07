package sample.DAO;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoDatabase;
import sample.Log.Log;
import sample.interfaces.DAOConexio;

/**
 * @author Francesc Banzo
 *
 */
public class DAOConexioMongoDB implements DAOConexio {
    /**
     * @param mongoClient Variable amb la que crearem la connexio
     * @param db Variable string que emmagatzemara el nom de la db
     * @param database Variable amb la que agafarem la db
     */
    private static MongoClient mongoClient;
    private static MongoDatabase database;
    private String db;

    public DAOConexioMongoDB() {
    }

    public String getDb() {
        return db;
    }

    public void setDb(String db) {
        this.db = db;
    }

    /**
     * Obre la connexio i agafa la db que utilitzara el programa
     */
    public void open() {
        // Creem la connexio amb la base de dades que t'ha pasat l'usuari
        try {
            mongoClient = MongoClients.create();
            database = mongoClient.getDatabase(db);
            System.out.println("Conexio oberta correctament");

        } catch (IllegalArgumentException e) {
            System.err.println("La connexió no s'ha pogut realiztar correctament");

            String text = "open DAOConexioMongoDB: \n";
            Log.save(text, e);

        } catch (Exception e) {
            System.err.println("La connexió no s'ha pogut realiztar correctament");

            String text = "open DAOConexioMongoDB: \n";
            Log.save(text, e);
        }
    }

    /**
     * Tanca la connexio
     */
    public void close() {
        try {
            mongoClient.close();
            System.out.println("Connexio tancada correctament");

        } catch (Exception e) {
            System.err.println("Error al tancar la connexio");

            String text = "close DAOConexioMongoDB: \n";
            Log.save(text, e);
        }
    }

    public static MongoDatabase getDatabase() {
        return database;
    }

    public static void setDatabase(MongoDatabase database) {
        DAOConexioMongoDB.database = database;
    }
}
